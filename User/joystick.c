#include "global.h"
#include "lpc17xx_gpio.h"
#include "joystick.h"

#define APPUYE 0
#define NON_APPUYE 1

void Update_Joystick(){

	int port1 = GPIO_ReadValue(1);
	int port2 = GPIO_ReadValue(2);
	
	if((port1 & (1<<21)) == APPUYE){ //D
		etat_joystick_actuel = D;
	}
	else if((port1 & (1<<20)) == APPUYE){ // CTR
		etat_joystick_actuel = CTR;
	}
	else if((port2 & (1<<8)) == APPUYE){ // A
		etat_joystick_actuel = A;
	}
	else if((port2 & (1<<12)) == APPUYE){ // B
		etat_joystick_actuel = B;
	}
	else if((port2 & (1<<13)) == APPUYE){ // C
		etat_joystick_actuel = C;
	}
	else {
		etat_joystick_actuel = RAS; // RAS
	}
}








