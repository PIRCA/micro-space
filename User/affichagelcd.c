#include "affichagelcd.h"
#include "touch\ili_lcd_general.h"								// write_data
#include "global.h"
#include "images.h"

unsigned short COLORS[4] = {COLOR_1, COLOR_2, COLOR_3, COLOR_4}; // Couleurs des sprites
char final[16];


/**
 * @brief Rassemble quatre valeurs dans un seul unsigned char.
 *
 * Cette fonction prend quatre valeurs unsigned char et les rassemble dans un seul unsigned char.
 * Les valeurs sont rassemblées dans l'ordre suivant : val1, val2, val3, val4.
 * Chaque valeur est décalée vers la gauche par un nombre spécifique de bits, puis combinée à l'aide de l'opérateur OU binaire.
 *
 * @param val1 La première valeur à rassembler.
 * @param val2 La deuxième valeur à rassembler.
 * @param val3 La troisième valeur à rassembler.
 * @param val4 La quatrième valeur à rassembler.
 * @return La valeur rassemblée en tant qu'unsigned char.
 */
unsigned char packValues(unsigned char val1, unsigned char val2, unsigned char val3, unsigned char val4)
{
	return (val1 << 6) | (val2 << 4) | (val3 << 2) | val4;
}


/**
 * @brief Décompresse une valeur empaquetée et la stocke dans un pointeur.
 * 
 * Cette fonction décompresse une valeur empaquetée en utilisant un masque de bits
 * et la stocke dans un pointeur spécifié.
 * 
 * @param packedValue La valeur empaquetée à décompresser.
 * @param val Pointeur vers la variable où la valeur décompressée sera stockée.
 * @param pos La position de la valeur dans la valeur empaquetée.
 */
void unpackValues(unsigned char packedValue, unsigned char *val, char pos) {
    *val = (packedValue >> (8 - pos * 2)) & 0x03;
}


/**
 * @file
 * @brief Fonction pour inverser les bits d'un caractère.
 *
 * Cette fonction prend un caractère en entrée et inverse les bits de ce caractère.
 * Les 2 premiers bits de poids fort sont échangés avec les 2 bits de poids faible,
 * les 2 bits suivants sont échangés avec les 2 bits suivants, et ainsi de suite.
 *
 * @param entree Le caractère dont les bits doivent être inversés.
 * @return Le caractère avec les bits inversés.
 */
char inverser_bits(char entree)
{
	return ((entree & (3 << 4)) >> 2) | ((entree & (3 << 2)) << 2) | (entree >> 6) | (entree << 6);
}


/**
 * @brief Vérifie si les coordonnées (x, y) sont à l'intérieur de l'écran.
 * 
 * @param x La coordonnée x.
 * @param y La coordonnée y.
 * @return 1 si les coordonnées sont à l'intérieur de l'écran, 0 sinon.
 */
char est_dans_ecran(int x, int y)
{
	return ((x >= 0 && x < 240) && (y >= 0 && y < 320));
}


/**
 * Efface le buffer de l'affichage LCD avec une couleur de fond spécifiée.
 *
 * @param bg_color La couleur de fond à utiliser pour effacer le buffer.
 */
void effacer_buffer(char bg_color)
{
	short h, w;
	char color = packValues(bg_color, bg_color, bg_color, bg_color);
	for (h = 0; h < 320; h++) {
		for (w = 0; w < 240 / 4; w++) {
			Frame_buffer[w + h * 240 / 4] = color;
		}
	}
}


/**
 * @brief Récupère une tuile à partir d'un index et d'une symétrie.
 * 
 * @param N L'index de la tuile.
 * @param S La symétrie de la tuile.
 */
void recuperer_tile(int N, SYM S)
{
	int M = (N / 30) * 480 + 2 * (N % 30);
	char k;
	int E;
	int K;
	
	for (k = 0; k < 16; k++)
	{
		if (S == 1)
		{
			final[k] = inverser_bits(image_data_Tile_map_lois[M + 60 * (k / 2) + ((k+1) % 2)]);
			final[k + 1] = inverser_bits(image_data_Tile_map_lois[M + 60 * (k / 2) + (k % 2)]);
		}
		else if (S == 2)
		{
			K = (15-k);
			E =  K - (K%2) + (K+1)%2;
			final[k] = image_data_Tile_map_lois[M + 60 * (E / 2) + (E % 2)];
		}
		else
		{
			final[k] = image_data_Tile_map_lois[M + 60 * (k / 2) + (k % 2)];
		}
	}
}


/**
 * @brief Dessine un sprite sans fond à une position donnée sur l'écran.
 * 
 * @param S Le sprite à dessiner.
 * @param X La position horizontale du sprite sur l'écran.
 * @param Y La position verticale du sprite sur l'écran.
 * @param wrap Indique si le sprite doit être enroulé sur l'écran lorsque sa position dépasse les limites de l'écran.
 */
void dessiner_sprite_sans_fond(const SPRITE S, int X, int Y, Bool wrap)
{
	int h, w, wrappedX, wrappedY, W, H;
	char pos, remainder, shift;
	unsigned char color;
	unsigned int frameIndex;

	int i, j;
	int index_spacecraft_tile;
	short number_tile;
	SYM sym;

	for (j = 0; j < S.tile_hauteur; j++)
	{
		for (i = 0; i < S.tile_largeur; i++)
		{
			index_spacecraft_tile = i + j * S.tile_largeur;
			number_tile = S.sprite_tiles[index_spacecraft_tile];
			sym = (SYM)S.sym_tiles[index_spacecraft_tile];
			recuperer_tile(number_tile, sym);
			
			for (h = 0; h < 8; h++)
			{
				
				for (w = 0; w < 8; w++)
				{
					W = w;
					H = h;

					wrappedX = (wrap == TRUE) ? ((X + W+(i*8)) % screenWidth < 0) ? screenWidth + (X + W+(i*8)) % screenWidth : (X + W+(i*8)) % screenWidth : X + W +(i*8);
					wrappedY = (wrap == TRUE) ? ((Y + H+(j*8)) % screenHeight < 0) ? screenHeight + (Y + H+(j*8)) % screenHeight : (Y + H+(j*8)) % screenHeight : Y + H +(j*8);

					if (wrap || est_dans_ecran(wrappedX, wrappedY))
					{
						int pixelIndex = (w + h * 8) / 4;

						frameIndex = (wrappedY * screenWidth + wrappedX) / 4;
						remainder = W % 4;
						pos = (remainder == 0 ? 1 : (remainder == 1 ? 2 : (remainder == 2 ? 3 : 4)));

						unpackValues(final[pixelIndex], &color, pos);
						if (color != 3)
						{
							shift = 6 - (wrappedX % 4) * 2;
							Frame_buffer[frameIndex] = (Frame_buffer[frameIndex] & ~(0x03 << shift)) | (color << shift);
						}
					}
				}
			}
		}
	}	
}


/**
 * @brief Dessine une tuile dans la mémoire du framebuffer.
 * 
 * Cette fonction dessine une tuile à la position spécifiée (X, Y) dans la mémoire du framebuffer.
 * La tuile est dessinée en utilisant les données finales fournies.
 * 
 * @param X La position horizontale de la tuile.
 * @param Y La position verticale de la tuile.
 */
void dessiner_tile_in_memory(int X, int Y)
{
	int D = (60 * Y) + (X / 4);
	char k;
	unsigned char shift;
	unsigned char mask;
	unsigned char value;

	switch (X % 4)
	{
	case 0:
		for (k = 0; k < 8; k++)
		{
			Frame_buffer[D + 60 * k] = final[2 * k];
			Frame_buffer[D + 60 * k + 1] = final[2 * k + 1];
		}
		break;
	case 1:
		for (k = 0; k < 8; k++)
		{
			shift = (k % 2 == 0) ? 6 : 0;
			mask = (k % 2 == 0) ? 0x03 : 0xC0;
			value = (final[k * 2] >> shift) | (final[k * 2 + 1] << (8 - shift));
			Frame_buffer[D + k * 60] = (Frame_buffer[D + k * 60] & ~mask) | value;
		}
		break;
	case 2:
		for (k = 0; k < 8; k++)
		{
			shift = (k % 2 == 0) ? 4 : 2;
			mask = (k % 2 == 0) ? 0x0F : 0xF0;
			value = (final[k * 2] >> shift) | (final[k * 2 + 1] << (8 - shift));
			Frame_buffer[D + k * 60] = (Frame_buffer[D + k * 60] & ~mask) | value;
		}
		break;
	case 3:
		for (k = 0; k < 8; k++)
		{
			shift = (k % 2 == 0) ? 2 : 4;
			mask = (k % 2 == 0) ? 0x3F : 0xC0;
			value = (final[k * 2] >> shift) | (final[k * 2 + 1] << (8 - shift));
			Frame_buffer[D + k * 60] = (Frame_buffer[D + k * 60] & ~mask) | value;
		}
		break;
	default:
		break;
	}
}


/**
 * @brief Dessine une tuile à une position donnée sur l'écran LCD.
 * 
 * Cette fonction récupère une tuile spécifique avec un identifiant N et un symbole S,
 * puis la dessine à la position (X, Y) sur l'écran LCD.
 * 
 * @param X La coordonnée X de la position où la tuile doit être dessinée.
 * @param Y La coordonnée Y de la position où la tuile doit être dessinée.
 * @param N L'identifiant de la tuile à dessiner.
 * @param S Le symbole de la tuile à dessiner.
 */
void dessiner_tile(int X, int Y, int N, SYM S)
{
	recuperer_tile(N, S);
	dessiner_tile_in_memory(X, Y);
}


/**
 * @brief Charge le contenu du Frame_buffer dans la mémoire GRAM de l'afficheur LCD.
 * 
 * Cette fonction parcourt le Frame_buffer et écrit les valeurs correspondantes dans la mémoire GRAM de l'afficheur LCD.
 * Chaque pixel est représenté par 4 bits, donc chaque octet du Frame_buffer contient 2 pixels.
 * Les valeurs des pixels sont décompressées à l'aide de la fonction unpackValues() et sont ensuite écrites dans la mémoire GRAM.
 * Les couleurs des pixels sont obtenues à partir du tableau COLORS.
 * 
 * @note Cette fonction suppose que le Frame_buffer est correctement initialisé et que la mémoire GRAM est accessible.
 */
void charger_GRAM()
{
	int h, w;
	unsigned char var1;
	unsigned char var2;
	unsigned char var3;
	unsigned char var4;
	for (h = 0; h < 320; h++)
	{
		for (w = 0; w < 240 / 4; w++)
		{
			unpackValues(Frame_buffer[w + h * 240 / 4], &var1, 1);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var2, 2);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var3, 3);
			unpackValues(Frame_buffer[w + h * 240 / 4], &var4, 4);
			write_data(COLORS[var1]);
			write_data(COLORS[var2]);
			write_data(COLORS[var3]);
			write_data(COLORS[var4]);
		}
	}
}


/**
 * @brief Dessine la tilemap.
 * 
 * Cette fonction parcourt une matrice de 20 lignes et 30 colonnes pour dessiner chaque tuile à l'écran.
 * Chaque tuile est dessinée à une position spécifique en fonction de ses coordonnées dans la matrice.
 * Les tuiles sont numérotées de 0 à 599 et sont dessinées à l'aide de la fonction dessiner_tile().
 * Les tuiles des lignes 20 à 39 sont toutes dessinées avec le même numéro de tuile (0) et le même symbole (NO_SYM).
 */
void dessiner_tilemap()
{
	int h, w;
	int N;
	for (h = 0; h < 20; h++)
	{
		for (w = 0; w < 30; w++)
		{
			N = w + h * 30;
			dessiner_tile(w * 8, h * 8, N, NO_SYM);
		}
	}
	for (h = 20; h < 40; h++)
	{
		for (w = 0; w < 30; w++)
		{
			dessiner_tile(w * 8, h * 8, 0, NO_SYM);
		}
	}
}
	

/**
 * @brief Dessine le joueur sur l'écran LCD en fonction de la position et de l'index donnés.
 * 
 * @param X La position horizontale du joueur sur l'écran.
 * @param Y La position verticale du joueur sur l'écran.
 * @param i L'index utilisé pour déterminer le sprite à afficher.
 */
void dessiner_joueur(unsigned short X, unsigned short Y ,int i){
	if (i <= 2) {
		dessiner_sprite_sans_fond(spacecraft_frame_1,X,Y,TRUE);
	}
	else if (i <= 5 && i > 2) {
		dessiner_sprite_sans_fond(spacecraft_frame_2,X,Y+1,TRUE);
	}
	else if (i <= 8 && i > 5) {
		dessiner_sprite_sans_fond(spacecraft_frame_3,X,Y+1,TRUE);
	}
	else if (i <= 11 && i > 8) {
		dessiner_sprite_sans_fond(spacecraft_frame_4,X,Y-1,TRUE);
	}
	else if (i <= 14 && i > 11) {
		dessiner_sprite_sans_fond(spacecraft_frame_5,X,Y-1,TRUE);
	}
}


/**
 * @brief Dessine l'historique des sprites en fonction de leur identifiant et de leur hauteur.
 * 
 * Cette fonction parcourt l'historique des sprites et dessine chaque sprite en fonction de son identifiant et de sa hauteur.
 * Les sprites sont dessinés en utilisant la fonction dessiner_sprite_sans_fond.
 * Les sprites peuvent être de différents types, tels que Champ_meteorites, Sombronces et Sablieres.
 * L'identifiant de chaque sprite détermine la couleur et la position du sprite sur l'écran.
 * 
 * @param Aucun
 * @return Aucun
 */
void dessiner_historique(void){
	int i;
	unsigned char id_sort;
	short hauteur;
	const SPRITE *sprite = &Champ_meteorites;
	for (i = 0; i < 10; i++){
		hauteur = historique[i].hauteur;
		id_sort = historique[i].id_sort;
		if (historique[i].var_affichage == 1){
			sprite = &Sombronces;
		} else {
			sprite = &Champ_meteorites;
		}
			switch (id_sort){
			case 21 :
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_sans_fond(*sprite,COL4,hauteur,FALSE);
				break;
			case 22 :
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_sans_fond(*sprite, COL3, hauteur, FALSE);
				break;
			case 23:
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_sans_fond(*sprite, COL2, hauteur, FALSE);
				break;
			case 24:
				if (historique[i].var_affichage == 1)
				{
					sprite = &Sombronces;
				}
				else
				{
					sprite = &Champ_meteorites;
				}
				dessiner_sprite_sans_fond(*sprite, COL1, hauteur, FALSE);
				break;
			case 41 : 
				sprite = &Sablieres;
				dessiner_sprite_sans_fond(*sprite,COL3,hauteur,FALSE);
				break;
			case 43 :
				sprite = &Sablieres;
				dessiner_sprite_sans_fond(*sprite, COL2, hauteur, FALSE);
				break;
			case 46 :
				sprite = &Sablieres;
				dessiner_sprite_sans_fond(*sprite, COL1, hauteur, FALSE);
				break;
			case 42 : 
				dessiner_sprite_sans_fond(Champ_meteorites,COL2,hauteur,FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites,COL4,hauteur,FALSE);
				break;
			case 44 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 45 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL3, hauteur, FALSE);
				break;
			case 71 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL3, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 72 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL3, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 73 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL4, hauteur, FALSE);
				break;
			case 74 :
				dessiner_sprite_sans_fond(Champ_meteorites, COL1, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL2, hauteur, FALSE);
				dessiner_sprite_sans_fond(Champ_meteorites, COL3, hauteur, FALSE);
				break;
			default :
				break;
		}
	}
}


/**
 * @brief Dessine un sprite avec un fond à la position spécifiée.
 * 
 * Cette fonction parcourt les tuiles du sprite et les dessine à la position (X, Y) en utilisant la fonction dessiner_tile().
 * Chaque tuile du sprite est représentée par un symbole et une valeur de sprite_tiles.
 * 
 * @param sprite Le sprite à dessiner.
 * @param X La position horizontale du sprite.
 * @param Y La position verticale du sprite.
 */
void dessiner_sprite_avec_fond(const SPRITE sprite, int X, int Y){
	int i, j;
	unsigned char sprite_hauteur = sprite.tile_hauteur;
	unsigned char sprite_largeur = sprite.tile_largeur; 
	for (j = 0; j < sprite_hauteur; j++){
		for (i = 0; i < sprite_largeur; i++){
			dessiner_tile(X + i * 8, Y + j * 8, sprite.sprite_tiles[i + j * sprite_largeur], (SYM)sprite.sym_tiles[i + j * sprite_largeur]);
		}
	}
}


/**
 * @brief Dessine le header du buffer avec une couleur spécifiée.
 * 
 * Cette fonction parcourt le buffer de frame et remplit chaque pixel avec la couleur spécifiée.
 * Le header est dessiné en utilisant la fonction dessiner_sprite_avec_fond().
 * 
 * @param color La couleur à utiliser pour remplir le buffer.
 */
void dessiner_buffer_header(){
	dessiner_sprite_sans_fond(Score,11*8,0,FALSE);
}


/**
 * @brief Dessine un chiffre sur l'écran LCD à une position donnée.
 * 
 * @param X La coordonnée X de la position du chiffre sur l'écran.
 * @param Y La coordonnée Y de la position du chiffre sur l'écran.
 * @param chiffre Le chiffre à dessiner (entre 0 et 9).
 */
void dessiner_chiffre(int X, int Y, int chiffre){
	switch (chiffre){
		case 0 :
			dessiner_tile(X,Y,90,NO_SYM);
			break;
		case 1 :
			dessiner_tile(X,Y,91,NO_SYM);
			break;
		case 2 :
			dessiner_tile(X,Y,92,NO_SYM);
			break;
		case 3 :
			dessiner_tile(X,Y,120,NO_SYM);
			break;
		case 4 :
			dessiner_tile(X,Y,121,NO_SYM);
			break;
		case 5 :
			dessiner_tile(X,Y,122,NO_SYM);
			break;
		case 6 :
			dessiner_tile(X,Y,150,NO_SYM);
			break;
		case 7 :
			dessiner_tile(X,Y,151,NO_SYM);
			break;
		case 8 :
			dessiner_tile(X,Y,152,NO_SYM);
			break;
		case 9 :
			dessiner_tile(X,Y,180,NO_SYM);
			break;
		default :
			break;
	}
}


/**
 * @brief Dessine le score sur l'écran LCD.
 * 
 * Cette fonction prend un entier représentant le score et dessine chaque chiffre du score sur l'écran LCD.
 * Les chiffres sont affichés de droite à gauche, avec le chiffre des unités à l'extrême droite et le chiffre des dizaines à l'extrême gauche.
 * 
 * @param score Le score à afficher.
 */
void dessiner_score(int score){
	unsigned char unite = score % 10;
	unsigned char dizaine = (score / 10) % 10;
	unsigned char centaine = (score / 100) % 10;
	unsigned char millier = (score / 1000) % 10;
	unsigned char dix_millier = (score / 10000) % 10;
	dessiner_chiffre(17*8,8,0);
	dessiner_chiffre(16*8,8,unite);
	dessiner_chiffre(15*8,8,dizaine);
	dessiner_chiffre(14*8,8,centaine);
	dessiner_chiffre(13*8,8,millier);
	dessiner_chiffre(12*8,8,dix_millier);
}


/**
 * @brief Dessine une explosion à une position donnée sur l'écran.
 * 
 * @param X La coordonnée X de la position de l'explosion.
 * @param Y La coordonnée Y de la position de l'explosion.
 * @param i L'indice de l'animation de l'explosion.
 * 
 * Cette fonction dessine une explosion à la position spécifiée sur l'écran.
 * L'indice de l'animation détermine quelle image de l'explosion sera dessinée.
 * Les différentes images de l'explosion sont sélectionnées en fonction de la valeur de l'indice.
 * 
 * @note Cette fonction utilise des sprites sans fond pour dessiner l'explosion.
 */
void dessiner_explosion(int X, int Y,int i){
	if (i <= 2){
		dessiner_sprite_sans_fond(Prout_frame_1,X+28,Y+20,FALSE);
	} else if (i <= 5 && i > 2){
		dessiner_sprite_sans_fond(Prout_frame_2,X+20,Y+12,FALSE);
	} else if (i <= 8 && i > 5){
		dessiner_sprite_sans_fond(Prout_frame_3,X+16,Y+8,FALSE);
	} else if (i <= 11 && i > 8){
		dessiner_sprite_sans_fond(Prout_frame_4,X+12,Y+8,FALSE);
	} else if (i <= 14 && i > 11){
		dessiner_sprite_sans_fond(Prout_frame_5,X+4,Y,FALSE);
	} else if (i <= 17 && i > 14){
		dessiner_sprite_sans_fond(Prout_frame_6,X,Y,FALSE);
	} else if (i <= 20 && i > 17){
		dessiner_sprite_sans_fond(Prout_frame_7,X,Y,FALSE);
	}
}


/**
 * @brief Dessine les lettres d'une phrase sur un écran LCD à partir des tuiles correspondantes.
 * 
 * @param X La position horizontale de départ.
 * @param Y La position verticale de départ.
 * @param phrase La phrase à afficher.
 */
void dessiner_lettres(int X, int Y, char *phrase)
{
	int i = 0;
	char caractere = phrase[i];

	while (caractere != '\0')
	{
		// Vérifier si le caractère est une lettre majuscule (entre 'A' et 'Z')
		if (caractere >= 'A' && caractere <= 'Z')
		{
			switch (caractere)
			{
				case 'A':
					dessiner_tile(X + i * 8, Y, 181, NO_SYM);
					break;
				case 'B':
					dessiner_tile(X + i * 8, Y, 182, NO_SYM);
					break;
				case 'C':
					dessiner_tile(X + i * 8, Y, 210, NO_SYM);
					break;
				case 'D':
					dessiner_tile(X + i * 8, Y, 211, NO_SYM);
					break;
				case 'E':
					dessiner_tile(X + i * 8, Y, 212, NO_SYM);
					break;
				case 'F':
					dessiner_tile(X + i * 8, Y, 240, NO_SYM);
					break;
				case 'G':
					dessiner_tile(X + i * 8, Y, 241, NO_SYM);
					break;
				case 'H':
					dessiner_tile(X + i * 8, Y, 242, NO_SYM);
					break;
				case 'I':
					dessiner_tile(X + i * 8, Y, 270, NO_SYM);
					break;
				case 'J':
					dessiner_tile(X + i * 8, Y, 271, NO_SYM);
					break;
				case 'K':
					dessiner_tile(X + i * 8, Y, 272, NO_SYM);
					break;
				case 'L':
					dessiner_tile(X + i * 8, Y, 300, NO_SYM);
					break;
				case 'M':
					dessiner_tile(X + i * 8, Y, 301, NO_SYM);
					break;
				case 'N':
					dessiner_tile(X + i * 8, Y, 302, NO_SYM);
					break;
				case 'O':
					dessiner_tile(X + i * 8, Y, 330, NO_SYM);
					break;
				case 'P':
					dessiner_tile(X + i * 8, Y, 331, NO_SYM);
					break;
				case 'Q':
					dessiner_tile(X + i * 8, Y, 332, NO_SYM);
					break;
				case 'R':
					dessiner_tile(X + i * 8, Y, 360, NO_SYM);
					break;
				case 'S':
					dessiner_tile(X + i * 8, Y, 361, NO_SYM);
					break;
				case 'T':
					dessiner_tile(X + i * 8, Y, 362, NO_SYM);
					break;
				case 'U':
					dessiner_tile(X + i * 8, Y, 390, NO_SYM);
					break;
				case 'V':
					dessiner_tile(X + i * 8, Y, 391, NO_SYM);
					break;
				case 'W':
					dessiner_tile(X + i * 8, Y, 392, NO_SYM);
					break;
				case 'X':
					dessiner_tile(X + i * 8, Y, 420, NO_SYM);
					break;
				case 'Y':
					dessiner_tile(X + i * 8, Y, 421, NO_SYM);
					break;
				case 'Z':
					dessiner_tile(X + i * 8, Y, 422, NO_SYM);
					break;
				case ' ':
					dessiner_tile(X + i * 8, Y, 0, NO_SYM);
					break;
				default:
					break;
			}
		}
		i++;
		caractere = phrase[i];
	}
}


/**
 * @brief Dessine le menu à l'écran.
 * 
 * Cette fonction utilise la fonction `dessiner_sprite_avec_fond` pour afficher le menu à l'écran.
 * Le menu est représenté par le sprite `Menu` et est affiché avec une taille de 24x24 pixels.
 */
void dessiner_menu(){
	dessiner_sprite_avec_fond(Menu,24,24);
}


/**
 * @file affichagelcd.c
 * @brief Fonction pour dessiner le menu du message sur l'écran LCD.
 */

/**
 * @brief Dessine le menu du message sur l'écran LCD.
 * 
 * Cette fonction dessine le menu du message sur l'écran LCD en utilisant des tuiles et des lettres.
 * Le menu affiche un message demandant à l'utilisateur d'appuyer sur les touches gauche et droite pour démarrer.
 */
void dessiner_menu_message(){
	int w;
	for (w = 4; w < 26; w++){
		dessiner_tile(w*8,30*8,454,NO_SYM);
		dessiner_tile(w*8,36*8,454,NO_SYM);
	}
	dessiner_lettres(5*8,32*8,"PRESS LEFT AND RIGHT");
	dessiner_lettres(11*8,34*8,"TO START");
}
