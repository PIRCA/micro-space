#ifndef HISTORIQUE_H
#define HISTORIQUE_H

#include "global.h"

void init_histo(void);
void ajouter_histo(el_histo);
void maj(void);
void collision(void);
void ajout_nouveau_sort(void);
Bool espacement_sort(void);

#endif
