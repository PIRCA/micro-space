#include "communicationbluetooth.h"
#include "lpc17xx_pinsel.h"

void InitialisationBluetooth(){
	UART_CFG_Type maConfig; // variable qui accueille notre configuration
	PINSEL_CFG_Type mesPIN;
	
	// Configuration de l'UART 
	maConfig.Baud_rate = 38400; // 38400 bauds (changement de valeur du signal toutes les 2 microsecondes)
	maConfig.Databits = UART_DATABIT_8; // Donn�es de transmission sur 1 octet (valeur par d�faut)
	maConfig.Parity = UART_PARITY_NONE; // Valeur par d�faut, pas de parit� ici
	maConfig.Stopbits = UART_STOPBIT_1;  // Par d�faut
	
	UART_Init(LPC_UART0, &maConfig);  // Configuration de l'UART 
	

	// Configuration de P0.2	en mode TXD0
	mesPIN.Portnum = PINSEL_PORT_0;	// Port 0
	mesPIN.Pinnum = PINSEL_PIN_2;		// Pin 2 
	mesPIN.Funcnum = PINSEL_FUNC_1;	// Fonction quand P0.2 = 01 (TXD0)
	//mesPIN.Pinmode = PINSEL_PINMODE_TRISTATE;	// Pas n�cessaire (avec le Pinsel ce sera fait tout seul)
	mesPIN.OpenDrain = PINSEL_PINMODE_NORMAL;	// Pin in the normal mode
	
	PINSEL_ConfigPin(&mesPIN);
	
	// Configuration de P0.3 en mode RXD0
	mesPIN.Pinnum = PINSEL_PIN_3;	//M�me config que P0.2 mais au Pin 3
	
	PINSEL_ConfigPin(&mesPIN);
	
	
	UART_TxCmd(LPC_UART0,ENABLE); //Transmission de messages autoris�e
	
	NVIC_EnableIRQ(UART0_IRQn);	// Autorisation des interruptions
	UART_IntConfig(LPC_UART0, UART_INTCFG_RBR,ENABLE);
}


// Fonction d'interruption pour l'UART0
void UART0_IRQHandler(void) { 
	Flag_message = TRUE;	// Indicateur qu'un message est re�u
	MessageRecu = UART_ReceiveByte(LPC_UART0);	// Sauvegarde du message dans une variable
}
