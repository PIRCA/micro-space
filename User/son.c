/*===========================================================================================================================*/
/* INCLUSION DES FICHIERS UTILES */
/*===========================================================================================================================*/

#include "lpc17xx_gpio.h"
#include "lpc17xx_timer.h"
#include "global.h"
#include "son.h"													// Tableau contenant la musique � jouer
#include "notes.h"

/*===========================================================================================================================*/
// D�claration des variables
/*===========================================================================================================================*/

Bool FlagTimer = FALSE ;															// �tat du timer							
int NoteFaite = 0;


TIM_MATCHCFG_Type speakermatch ; 											// D�claration de la variable speakermatch
TIM_MATCHCFG_Type timer1match ; 											// D�claration de la variable timer1match

int i =0;;

const float midi_to_frequency[128] = {
	8.18, 8.66, 9.18, 9.72, 10.30, 10.91, 11.56, 12.25, 12.98, 13.75, 14.57, 15.43,								// 0-11
	16.35, 17.32, 18.35, 19.45, 20.60, 21.83, 23.12, 24.50, 25.96, 27.50, 29.14, 30.87,							// 12-23
	32.70, 34.65, 36.71, 38.89, 41.20, 43.65, 46.25, 49.00, 51.91, 55.00, 58.27, 61.74,							// 24-35
	65.41, 69.30, 73.42, 77.78, 82.41, 87.31, 92.50, 98.00, 103.83, 110.00, 116.54, 123.47,						// 36-47
	130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185.00, 196.00, 207.65, 220.00, 233.08, 246.94,				// 48-59
	261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392.00, 415.30, 440.00, 466.16, 493.88,				// 60-71
	523.25, 554.37, 587.33, 622.25, 659.25, 698.46, 739.99, 783.99, 830.61, 880.00, 932.33, 987.77,				// 72-83
	1046.50, 1108.73, 1174.66, 1244.51, 1318.51, 1396.91, 1479.98, 1567.98, 1661.22, 1760.00, 1864.66, 1975.53, // 84-95
	2093.00, 2217.46, 2349.32, 2489.02, 2637.02, 2793.83, 2959.96, 3135.96, 3322.44, 3520.00, 3729.31, 3951.07, // 96-107
	4186.01, 4434.92, 4698.63, 4978.03, 5274.04, 5587.65, 5920.00, 6271.93, 6644.88, 7040.00, 7458.62, 7902.13	// 108-119
};

float midi_to_freq(int midi_note)
{
	if (midi_note < 0 || midi_note >= 128)
	{
		return 0.0f; // Note MIDI invalide
	}
	return midi_to_frequency[midi_note];
}

/*===========================================================================================================================*/
/* PROGRAMMATION DU SPEAKER POUR LA MUSIQUE DE FOND (TIMER0) */
/*===========================================================================================================================*/

void InitSpeaker(){
	TIM_TIMERCFG_Type speaker ; 													// D�claration de la variable myconfig
	
	GPIO_SetDir(0, 1<<26, 1);															// on met le speaker en sortie
	speaker.PrescaleOption = TIM_PRESCALE_USVAL ;					// configuration du registre PR -> valeur en microseconde
	speaker.PrescaleValue = 1	; 													// Pr�cision � la microseconde
	
	TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &speaker) ;				// configuration du Timer
	
	/* CONFIGURATION DE LA VARIABLE speakermatch */
	speakermatch.MatchChannel = 0 ;
	speakermatch.ResetOnMatch = ENABLE ;									// Reset sur le match autoris�
	speakermatch.StopOnMatch = DISABLE ;									// Stop sur le match non autoris�
	speakermatch.IntOnMatch = ENABLE ;
	speakermatch.MatchValue = 10 ;												// valeur du match
	speakermatch.ExtMatchOutputType = 0 ;
	
	TIM_ConfigMatch(LPC_TIM0, &speakermatch) ;						// Configuration du match
	
	NVIC_EnableIRQ(TIMER0_IRQn) ; 												// Autorisation des interruptions pour le timer0
	
	TIM_Cmd(LPC_TIM0, ENABLE) ; 													// Demarrage du Timer
}

/*===========================================================================================================================*/
/* ROUTINE D'INTERRUPTION DU TIMER0 */ 
/*===========================================================================================================================*/

void TIMER0_IRQHandler(){																// Nom de fonction � respecter sinon pas d'int�rruption possible
	
	if(!FlagTimer){
		GPIO_SetValue(0, 1 << 26) ;
		FlagTimer = TRUE ;
	}
	else {
		GPIO_ClearValue(0, 1 << 26) ;
		FlagTimer = FALSE ;
	}
	
	TIM_ClearIntPending(LPC_TIM0,TIM_MR0_INT) ; 						// Acquittement (sinon interruption bizarre : non respect des 10ms)
}

/*===========================================================================================================================*/
/* PROCEDURE POUR JOUER UNE NOTE EN UTILISANT LE MATCH DU TIMER0 */
/*===========================================================================================================================*/

void JouerUneNote(float f){

   	speakermatch.MatchValue = (int)((1/f)*1000000);				// jouer la note en mettant le timer dans la bonne unit� 
	
		TIM_ConfigMatch(LPC_TIM0,&speakermatch);
   	TIM_ResetCounter(LPC_TIM0);   	 											// On reset le timer	 
		TIM_Cmd(LPC_TIM0, ENABLE);   	 												// On allume le timer 	
}

/*===========================================================================================================================*/
/* INITIALISATION DU TIMER 1 : permet de jouer la musique en int�rruption */
/*===========================================================================================================================*/

void InitTimer1(){
	TIM_TIMERCFG_Type timer1 ; 														// D�claration de la variable timer1
	
	timer1.PrescaleOption = TIM_PRESCALE_USVAL ;					// configuration du registre PR -> valeur en microseconde
	timer1.PrescaleValue = 1	; 													// Pr�cision � la microseconde
	
	TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &timer1) ;					// configuration du Timer
	
	/* CONFIGURATION DE LA VARIABLE timermatch */
	timer1match.MatchChannel = 1 ;
	timer1match.ResetOnMatch = ENABLE ;										// Reset sur le match autoris�
	timer1match.StopOnMatch = DISABLE ;										// Stop sur le match non autoris�
	timer1match.IntOnMatch = ENABLE ;
	timer1match.MatchValue = FTL_music[1];							// valeur du match
	timer1match.ExtMatchOutputType = 0 ;
	
	TIM_ConfigMatch(LPC_TIM1, &timer1match) ;							// Configuration du match
	TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
	
	NVIC_EnableIRQ(TIMER1_IRQn) ; 												// Autorisation des interruptions pour le timer0
	
	TIM_Cmd(LPC_TIM1, ENABLE) ; 													// D�marrage du Timer
}


/*===========================================================================================================================*/
/* ROUTINE D'INTERRUPTION DU TIMER1 */ 
/*===========================================================================================================================*/

void TIMER1_IRQHandler(){
	if(1){
		timer1match.MatchValue = vitesse_musique*1.0*1000*FTL_music[NoteFaite+1] ; 
		TIM_ConfigMatch(LPC_TIM1,&timer1match);
		TIM_ResetCounter(LPC_TIM1);   	 											// On reset le timer	 
		TIM_Cmd(LPC_TIM1, ENABLE);   	 												// On allume le timer 
	
		JouerUneNote(4*midi_to_freq(FTL_music[NoteFaite]));
			
		NoteFaite = NoteFaite + 2 ; 
		if(NoteFaite > 511){
			NoteFaite = 0 ;					// on r�initialise le compteur
		}
	}
	TIM_ClearIntPending(LPC_TIM1,TIM_MR1_INT) ; 							// Acquittement 
}



