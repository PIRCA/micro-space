//===========================================================//
// Projet Micro - SNUM1 - ENSSAT - S2 2024   							   //
//===========================================================//
// File                : Micro_Space
// Hardware Environment: Open1768
// Build Environment   : Keil µVision
//===========================================================//

#include "main.h"
#include "globaldec.h"

//===========================================================//
// 										Function: Main
//===========================================================//

int main(void){
	multiples_init();
	while(1){
		while(!Flag_Start){
			if(Flag_capteur){
				ADC_CONTROL_REGISTER |= (1<<24); // On lance une acquisition MAINTENANT
				Flag_capteur = FALSE;
				lecture_ADC();
				acquisition_capteur();
				Update_Joystick();
				deplacement_joueur();
			}
			
			if (Flag_affichage){
				index_anim += 1;
				index_anim %= 40;
				effacer_buffer(0);
				dessiner_buffer_header();
				dessiner_menu();
				dessiner_joueur(85,34,index_anim%14);
				if (index_anim >= 20) {
					dessiner_menu_message();
				}
				charger_GRAM();
			}
		}
		init_start();
		while (!crash){
			if(Flag_capteur){
				ADC_CONTROL_REGISTER |= (1<<24); // On lance une acquisition MAINTENANT
				Flag_capteur = FALSE;
				lecture_ADC();
				acquisition_capteur();
				Update_Joystick();
				deplacement_joueur();
			}
			
			if(Flag_affichage){
				index_anim += 1;
				index_anim %= 15;
				effacer_buffer(0);
				dessiner_historique();
				dessiner_buffer_header(); // Dessine le header du buffer
				dessiner_score(score);
				dessiner_joueur(position_joueur,256,index_anim);
				charger_GRAM(); // (~)33.6 ms de latence pour écrire dans la GRAM de l'écran 
				collision();
				maj();
				if(vitesse_augmenter){maj();}
				Flag_affichage = FALSE;
			}
			
			if(Flag_mana){
				Flag_mana = FALSE;
				AffichageMana();
			}
			
			if(Flag_message){ // Si un message bluetooth est detecte (mis a 1 dans l'interruption)
				Flag_message = FALSE; // on repasse le messagebluetooth a 0
				DiminutionMana();
				ajout_nouveau_sort();
			}
		}
		vitesse_musique = 1;
		vitesse_augmenter = FALSE;
		effacer_buffer(0);
		index_anim = 0;
		while (crash && index_anim < 21){
			index_anim++;
			effacer_buffer(0);
			dessiner_historique();
			if (index_anim < 11){
				dessiner_joueur(position_joueur,256,index_anim);
			}
			
			dessiner_explosion(position_joueur-24,256-8,index_anim);
			charger_GRAM();
		}
		Flag_Start = FALSE;
	}
}


//---------------------------------------------------------------------------------------------	
#ifdef  DEBUG
void check_failed(uint8_t *file, uint32_t line) {while(1);}
#endif
