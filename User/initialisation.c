#include "initialisation.h"
#include "main.h"
#include "global.h"

void multiples_init()
{
	Flag_capteur = FALSE;			// Initialement on a aucune action a faire, les flag sont a false
	Flag_affichage = FALSE; 		// **
	Flag_mana = FALSE;				// **
	Flag_message = FALSE;			// **
	Flag_Start = FALSE;
	Capteur_contact = FALSE;	    // Initialement on considere que les capteurs sont au repos
	Capteur_moisissure = FALSE;	    // **
	vitesse_augmenter = FALSE;	    // Au depart la vitesse est normale

	sort = 0;						// L'identifiant de sort est a 0 pour signifier qu'aucun sort n'est lance
	incrementeur_capteur_mana = 1;	// On prepare la premiere interruption de capteur asap (10 ms)
	vitesse_musique = 1;			// Au depart la vitesse est normale
	init_ADC();				   		// **
	initialisation_capteur();  		// **
	lcd_Initializtion();	   		// Initialisation ecran assez longue, on la fait en premiere
	lcd_SetCursor(0, 0);	   		// **
	rw_data_prepare();		   		// **
	dessiner_buffer_header();  		// ** On prepare le header, sans faire l'affichage.
	InitialisationBluetooth(); 		// On lance le module Bluetooth
	AffichageMana();		   		// (Facultatif) On affiche le mana pour le joueur 2 une premiere fois
	InitSpeaker();			   		// Timer pour la musique, on lance la musique
	InitTimer1();			   		// **
	initialisation_timer();	   		// Timer capteur, mana, affichage. Le jeu commence
}

void init_start(){
	index_anim = 0;
	crash = FALSE;				   // On fait tourner le jeu avec crash a false
	mana = 5;					   // Le joueur 2 commence la partie avec 5 points de mana pour eviter une attente au debut
	score = 0;					   // Le score du joueur 1 est incremente avec le mana mais commence a 0
	position_joueur = 120;		   // Position centre droite pour commencer et multiple de 8 (important pour la gestion de la position)
	init_histo();				   // Uniquement de la preparation, ne lance rien, donc on le fait au debut
}

