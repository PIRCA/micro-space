#ifndef _BLUETOOTH_H
#define _BLUETOOTH_H

#include "lpc17xx_uart.h"	// UART pour le bluetooth 
#include "GestionMana.h"	// Gestion du mana (Diminution et Affichage ici)
#include "global.h"

// Fonctions de communicationbluetooth.c
void InitialisationBluetooth(void);
void UART0_IRQHandler(void); 

#endif
