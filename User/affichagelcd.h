#ifndef AFFICHAGELCD_H
#define AFFICHAGELCD_H

#include "constantes.h"

#define COL1 8
#define COL2 64
#define COL3 120
#define COL4 176



typedef enum {NO_SYM, V_SYM, H_SYM} SYM;

char est_dans_ecran(int x, int y);
void effacer_buffer(char bg_color);
void charger_GRAM(void);
void dessiner_tile(int X, int Y, int N, SYM S);
void dessiner_tilemap(void);
void dessiner_joueur(unsigned short X, unsigned short Y, int i);
void dessiner_historique(void);
void dessiner_buffer_header(void);
void dessiner_score(int score);
void dessiner_explosion(int X, int Y, int i);
void dessiner_lettres(int X, int Y, char *phrase);
void dessiner_menu(void);
void dessiner_menu_message(void);

#endif
