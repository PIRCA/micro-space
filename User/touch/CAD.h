#ifndef CAD_H
#define CAD_H

#define PCOMP *(uint32_t *)0x400FC0C4
#define ADC_INTERRUPT_REGISTER *(uint32_t *)0x4003400C

void init_ADC(void);
void lecture_ADC(void);

#endif
