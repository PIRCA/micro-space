def compact_array(data):
    compacted_data = []
    for i in range(0, len(data), 4):
        chunk = data[i:i+4]
        compacted_data.append(chunk[1]);
        compacted_data.append(chunk[3]);
    return compacted_data

# Read data from file
with open('entree.txt', 'r') as file:
    data_str = file.read()

# Extract the array name
start_index = data_str.find('char') + 13
end_index = data_str.find('[')
array_name = data_str[start_index:end_index].strip()

# Extract the array content
start_index = data_str.find('{') + 1
end_index = data_str.find('}')
array_data = data_str[start_index:end_index] # PROBLEM ? end_index + 1 pour inclure le dernier élément ? 

# Convert array data to list of integers
array_data = array_data.split(',')
array_data = [x.strip() for x in array_data] 

# Compact the array
compacted_data = compact_array(array_data)

decomposed_data = ""
compt = 0
max = len(compacted_data)
for i in range ((max//60)+1):
    decomposed_data += ', '.join(char for char in compacted_data[compt:compt+60]) + ',\n'
    compt += 60
decomposed_data = decomposed_data[:-2]

# Write compacted data to output file
with open('sortie.txt', 'w') as output_file:
    output_file.write(f'char unsigned {array_name}[{len(compacted_data)}] = \n{{{decomposed_data}}};')
    #output_file.write(f'char {array_name}[{len(compacted_data)}] = {{{", ".join(str(ord(char)) for char in compacted_data)}}};\n')
